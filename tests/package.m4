# Signature of the current package.
m4_define([AT_PACKAGE_NAME], [jupiter])
m4_define([AT_PACKAGE_TARNAME], [jupiter])
m4_define([AT_PACKAGE_VERSION], [0.1.0])
m4_define([AT_PACKAGE_STRING], [jupiter 0.1.0])
m4_define([AT_PACKAGE_BUGREPORT], [mulrich99@googlemail.com])
m4_define([AT_PACKAGE_URL], [])
